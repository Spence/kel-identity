Django==1.9.6
django-user-accounts==1.3.1
pinax-webanalytics==2.0.1
pinax-eventlog==1.1.1
pinax-theme-bootstrap==7.8.0
django-oauth-toolkit==0.10.0
